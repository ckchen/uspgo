  USPGo - Contributors
  ======================
 
  USPgo is a free open source project and everyone is more than welcome
  to contribute with either ideas, bug reports or source code.
  
  If you believe you can help, please read this document thoroughly.
  
  
  How to start
  ------------------
  
  If you want to contribute an idea or suggestion, or you have a specific
  inquiry, please feel free to contact the developers team by e-mail:
  
  uspgo@googlegroups.com
  
  If you want to start contributing code directly to the project, please, 
  read the section "General guidelines" bellow and good job!
  
  General guideles
  ------------------
  
  *This section needs to be detailed*
  
  1) Make suere your development tools do not conflict with UGPgo's 
     open source licesnse.
     
  2) Write and code in English so that potential contributors all around 
     the world can more easilty get involved.
     
  3) To report bugs or technical suggestions, raise (post) an issue in the
     corresponding project repository.
     
  3) To submit sporadic contributions, clone the relevant project and
     send a pull/merge request.
     
  
  
  

  
  
  
  