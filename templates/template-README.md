 USPgo <module>
 ============
 
 This software is a module of USPgo.
 
 The present project implements <project purpose>.

 
  About
  ------
  
  USPgo is an intelligent mobility application for car ride sharing.
  
  Currently, it features mobile app through which users can both request
  and offer rides. The online serivce process these demands and, if a 
  convenient match is found, hitchhiker and driver are connected. The
  app selects a meeting point and keep track of the ride progress. If
  everthing is ok, drivers may be rewarded virtual credits using
  blockchain technology.
 
  USPgo is free software.

  For licensing information, please refer to the companion file LICENSE.

  Further information may be found at https://gitlab.com/uspgo