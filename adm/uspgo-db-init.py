# uspgo-db-init.py - Initialize USGo database
#
#    Copyright (c) 2018 - CCSL-ICMC at USP 
#    
#                         https://gitlab.com/uspgo/uspgo/wikis/USPgo-Wiki
#    
#    This file is part of USPgo.
#
#    USPgo is free software: you can redistribute it and/or modify
#    it under the terms of the GNU General Public License as published by
#    the Free Software Foundation, either version 3 of the License, or
#    (at your option) any later version.
#
#    USPgo is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU General Public License for more details.
#
#    You should have received a copy of the GNU General Public License
#    along with Foobar.  If not, see <https://www.gnu.org/licenses/>.


# This script is used to initialize the database (config server, create tables,
# etc.) It's used to set-up every thing from the scratch, should we need to make
# a pristine install. For instance, let's say the database is corrupted, or we
# want to deploy a second instance of the system for tests purpose. In this case
# we might just run the script
#
# $ uspgo-db-init.py
#
# and nave the database all set with tables etc. ready to be utilized by the
# several USPgo modules.

from firebase import firebase
firebase = firebase.FirebaseApplication('https://uspgo-8f030.firebaseio.com/', None)

result = firebase.delete('', None)
print result